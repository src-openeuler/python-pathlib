Name:             python-pathlib
Version:          1.0.1
Release:          10
Summary:          Object-oriented filesystem paths
License:          MIT
URL:              https://pathlib.readthedocs.org/
Source0:          https://pypi.python.org/packages/source/p/pathlib/pathlib-%{version}.tar.gz
BuildArch:        noarch
BuildRequires:    python2-devel python2-sphinx

%description 
Manipulating filesystem paths as string objects can quichly become cumbersome;
multiple calls to os.path.join() or os.path.dirname(), etc. This module offers
a set of classes featuring all the common operations on paths in an easy,
object-oriented way.

%package -n python2-pathlib
Summary:          %summary
%{?python_provide:%python_provide python2-pathlib}

%description -n python2-pathlib 
Manipulating filesystem paths as string objects can quichly become cumbersome;
multiple calls to os.path.join() or os.path.dirname(), etc. This module offers
a set of classes featuring all the common operations on paths in an easy,
object-oriented way.

%prep
%autosetup -n pathlib-%{version} -p1
sphinx-build docs html

%build
%{__python2} setup.py build

%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%files -n python2-pathlib
%doc html README.txt LICENSE.txt
%{python2_sitelib}/*
%exclude %{_topdir}/BUILD/html/.{doctrees,buildinfo}

%changelog
* Thu Mar 5 2020 likexin <likexin4@huawei.com> - 1.0.1-10
- package init
